﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpCopyDirectory
{
    class Program
    {
        static void Main(string[] args)
        {
            var srcDir = "SrcDir";
            var dstDir = "DstDir";

            Directory.CreateDirectory(srcDir);
            DirectoryCopy(srcDir, dstDir, true);
        }

        private static void DirectoryCopy(string srcDirName, string dstDirName, bool copySubDirs)
        {
            // コピー元ディレクリが存在しない場合、終了します。
            if (!Directory.Exists(srcDirName)) return;

            // コピー先ディレクトリが存在しない場合
            if (!Directory.Exists(dstDirName))
            {
                // コピー先ディレクトリを作成します。
                Directory.CreateDirectory(dstDirName);
            }

            // コピー元ディレクトリ内のファイルを取得します。
            var srcDir = new DirectoryInfo(srcDirName);
            var srcFiles = srcDir.GetFiles();
            foreach (var srcFile in srcFiles)
            {
                // コピー元ディレクトリ内のファイルをコピー先ディレクトリにコピーします。
                var dstFile = Path.Combine(dstDirName, srcFile.Name);

                // コピー先に対象のファイルが存在する場合、次のファイルへ。
                if (File.Exists(dstFile)) continue;

                // ファイルをコピーします。
                srcFile.CopyTo(dstFile, false);
            }

            // サブディレクトリもコピーする場合
            if (copySubDirs)
            {
                // コピー元ディレクトリのサブディレクトリを取得します。
                var srcSubDirs = srcDir.GetDirectories();
                foreach (var srcSubDir in srcSubDirs)
                {
                    // このメソッドを再帰呼出しして、サブディレクトリをコピーします。
                    var dstDirPath = Path.Combine(dstDirName, srcSubDir.Name);
                    DirectoryCopy(srcSubDir.FullName, dstDirPath, copySubDirs);
                }
            }
        }
    }
}
